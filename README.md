# Leagueify Server

Comprehensive solution for managing sporting leagues, hosting API endpoints to seamlessly handle league administration, account management, player and team coordination, scheduling, and beyond.

This server is written in [Go][go-website] using the [Echo][echo-website] framework.

[go-website]: https://go.dev
[echo-website]: https://echo.labstack.com
